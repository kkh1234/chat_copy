Rails.application.routes.draw do
  resources :helloworldposts
  get "/login" , to:"session#login"
  post '/login',   to: 'session#create'
  get '/logout',  to: 'session#destroy'
  get "/", to:"session#index"
  root "session#index"

  
  get "/signup", to:"user#new"
  post "/signup", to:"user#create"
  
  get "/users/:id", to:"user#show"
  post "/users/:id", to:"user#uploadpic"

  get "/workspace", to:"workspace#index"

  get "/createchannel", to:"group#new"
  post "/createchannel", to:"group#create"

  get "/createworkspace", to:"workspace#create"
  post "/createworkspace", to:"workspace#create"
  get "/editworkspace", to:"workspace#new"
  
  get "/individualmessage", to:"individualmessage#index"
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
