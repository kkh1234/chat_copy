class AddCurrentworkspaceToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :currentworkspace, :integer
  end
end
